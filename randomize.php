<?php
/*

Plugin name: Randomize
Plugin URI: https://wordpress.org/plugins/randomize-for-elementor/
Description: Display randomized content using Elementor widgets.
Version: 1.0.0
Author: goINPUT
Author URI: https://goinput.de

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

function my_plugin_init(){
	randomize_textdomain_init();
	//randomize_gutenberg_enqueue();
}
 
//load textdomain - enable translation
function randomize_textdomain_init() {
    load_plugin_textdomain( 'randomize', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
  }
  add_action('init', 'my_plugin_init');

//Plugin activation handling - check requirements and show warnings for disabled functionality.
function randomize_activation() {
	
}
register_activation_hook(__FILE__, 'randomize_activation');


//------------------------------------------------ Elementor -------------------------------------------------------

// Check if elementor is present
function is_elementor_active() {
    return defined('ELEMENTOR_VERSION');
}

//Load Elementor functionality
function elementor_randomize() {
    require_once( dirname(__FILE__) . '/includes/elementor/elementor-randomize.php' );      // Load plugin file
    \GoINPUT_Randomize_Elementor\RandomizeElementor::instance();                    // And run the plugin

}
if(is_elementor_active()) {add_action( 'plugins_loaded', 'elementor_randomize' );}



//------------------------------------------------ Gutenberg -------------------------------------------------------
/*function randomize_gutenberg_enqueue() {
    wp_register_script(
        'randomize-block',
        plugins_url( 'block.js', __FILE__ ),
        array( 'wp-blocks', 'wp-element', 'wp-editor', 'wp-i18n', 'wp-components' ),
        filemtime( plugin_dir_path( __FILE__ ) . 'block.js' )
    );

    register_block_type( 'randomize/random-element', array(
        'editor_script' => 'randomize-block',
        'render_callback' => 'randomize_render_callback',
        'attributes' => array(
            'elements' => array(
                'type' => 'array',
                'default' => array(),
            ),
        ),
    ));
}

function randomize_render_callback( $attributes ) {
    $elements = $attributes['elements'];

    if ( count( $elements ) === 0 ) {
        return '';
    }

    $random_index = rand( 0, count( $elements ) - 1 );
    $random_element = $elements[ $random_index ];

    // Render the random element's content and InnerBlocks content
    return sprintf(
        '<div class="random-element">
            <div class="random-element-content">%s</div>
            <div class="random-element-inner">%s</div>
        </div>',
        $random_element['content'],
        $random_element['innerBlocks']
    );
}*/