<?php

namespace GoINPUT_Randomize_Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


/**
 * Elementor Randomize Widget.
 *
 * Elementor widget that inserts one or more items randomly chosen from a list.
 *
 * @since 2.0.0
 */
class Elementor_Randomize_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve Randomize widget name.
	 *
	 * @since 2.0.0
	 * @access public
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'randomize';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve Randomize widget title.
	 *
	 * @since 2.0.0
	 * @access public
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Randomize', 'randomize' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve oEmbed widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fa-solid fa-shuffle';
	}

	/**
	 * Get custom help URL.
	 *
	 * Retrieve a URL where the user can get more information about the widget.
	 *
	 * @since 1.0.0
	 * @access public
	 * @return string Widget help URL.
	 */
	public function get_custom_help_url() {
		return 'https://goinput.de/randomize';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the Randomize widget belongs to.
	 *
	 * @since 2.0.0
	 * @access public
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'general' ];
	}

	/**
	 * Get widget keywords.
	 *
	 * Retrieve the list of keywords the oEmbed widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 * @return array Widget keywords.
	 */
	public function get_keywords() {
		return [ 'random', 'shuffle', 'draw' ];
	}

	/**
	 * Register Randomize widget controls.
	 *
	 * Add input fields to allow the user to customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'randomize' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'list',
			[
				'label' => esc_html__( 'Item List', 'randomize' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => [
					[
						'name' => 'list_text',
						'label' => esc_html__( 'Text', 'randomize' ),
						'type' => \Elementor\Controls_Manager::TEXT,
						'default' => esc_html__( 'Text' , 'randomize' ),
						'label_block' => true,
					],
					[
						'name' => 'list_image',
						'label' => esc_html__( 'Image', 'randomize' ),
						'type' => \Elementor\Controls_Manager::MEDIA,
						'default' => [
                            'url' => \Elementor\Utils::get_placeholder_image_src(),
                        ],
						'show_label' => true,
					],
					[
						'name' => 'list_color',
						'label' => esc_html__( 'Color', 'randomize' ),
						'type' => \Elementor\Controls_Manager::COLOR,
					]
				],
				'default' => [
					[
						'list_text' => esc_html__( 'Random Item 1', 'randomize' ),
						'list_image' => \Elementor\Utils::get_placeholder_image_src(),
					],
					[
						'list_text' => esc_html__( 'Random Item 2', 'randomize' ),
						'list_image' => \Elementor\Utils::get_placeholder_image_src(),
					],
				],
				'title_field' => '{{{ list_text }}}',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style',
			[
				'label' => esc_html__( 'Style', 'randomize' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'alignment',
			[
				'type' => \Elementor\Controls_Manager::CHOOSE,
				'label' => esc_html__( 'Alignment', 'randomize' ),
				'options' => [
					'left' => [
						'title' => esc_html__( 'Left', 'randomize' ),
						'icon' => 'eicon-text-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'randomize' ),
						'icon' => 'eicon-text-align-center',
					],
					'right' => [
						'title' => esc_html__( 'Right', 'randomize' ),
						'icon' => 'eicon-text-align-right',
					],
				],
				'default' => 'center',
			]
		);

		$this->end_controls_section();

	}

	/**
	 * Render Randomize widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {

        $settings = $this->get_settings_for_display();

		$widget_id = $this->get_id();


        if(count($settings['list'])>0){
            $element = $settings['list'][rand(0,count($settings['list'])-1)];
            echo '<style>';
			echo '.randomized-text-' . $element['_id'] . '{color:' . $element['list_color'] . ';}';
			echo '.randomize-elementor-widget-'. $widget_id . '{display:flex; flex-wrap: wrap; justify-content:' . $settings['alignment'] . ';}';
			echo '</style>';
            echo '<div class="randomize-elementor-widget-'. $widget_id . '">';
            echo '<img src="' . $element['list_image']['url'] . '">';
            echo '<p class="randomized-text-' . $element['_id'] . '">' . $element['list_text'] . '</p>';
            echo '</div>';
        }
        else{
            echo '<div class="randomize-elementor-widget">';
            echo __('No items to randomize', 'randomize');
            echo '</div>';
        }



	}

}