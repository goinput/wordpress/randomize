<?php

namespace GoINPUT_Randomize_Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
} 


/**
 * Plugin class.
 *
 * The main class that initiates and runs the addon.
 *
 * @since 2.0.0
 */
final class RandomizeElementor {

	/**
	 * Addon Version
	 *
	 * @since 2.0.0
	 * @var string The addon version.
	 */
	const VERSION = '2.0.0';

	/**
	 * Minimum Elementor Version
	 *
	 * @since 2.0.0
	 * @var string Minimum Elementor version required to run the addon.
	 */
	const MINIMUM_ELEMENTOR_VERSION = '3.11.3';

	/**
	 * Minimum PHP Version
	 *
	 * @since 2.0.0
	 * @var string Minimum PHP version required to run the addon.
	 */
	const MINIMUM_PHP_VERSION = '7.3';

	/**
	 * Instance
	 *
	 * @since 2.0.0
	 * @access private
	 * @static
	 * @var \GoINPUT_Randomize_Elementor\RandomizeElementor The single instance of the class.
	 */
	private static $_instance = null;

	/**
	 * Instance
	 *
	 * Ensures only one instance of the class is loaded or can be loaded.
	 *
	 * @since 2.0.0
	 * @access public
	 * @static
	 * @return \GoINPUT_Randomize_Elementor\RandomizeElementor An instance of the class.
	 */
	public static function instance() {

		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;

	}

	/**
	 * Constructor
	 *
	 * Perform some compatibility checks to make sure basic requirements are met.
	 * If all compatibility checks pass, initialize the functionality.
	 *
	 * @since 2.0.0
	 * @access public
	 */
	public function __construct() {

		if ( $this->is_compatible() ) {
			add_action( 'elementor/init', [ $this, 'init' ] );
		}

	}

	/**
	 * Compatibility Checks
	 * Checks whether the site meets the addon requirement.
	 *
	 * @since 2.0.0
	 * @access public
	 */
	public function is_compatible() {

		// Check if Elementor installed and activated
		if ( ! did_action( 'elementor/loaded' ) ) {
			add_action( 'admin_notices', [ $this, 'admin_notice_missing_elementor_plugin' ] );
			return false;
		}

		// Check for required Elementor version
		if ( ! version_compare( ELEMENTOR_VERSION, self::MINIMUM_ELEMENTOR_VERSION, '>=' ) ) {
			add_action( 'admin_notices', [ $this, 'admin_notice_minimum_elementor_version' ] );
			return false;
		}

		// Check for required PHP version
		if ( version_compare( PHP_VERSION, self::MINIMUM_PHP_VERSION, '<' ) ) {
			add_action( 'admin_notices', [ $this, 'admin_notice_minimum_php_version' ] );
			return false;
		}

		return true;

	}

	/**
	 * Admin notice
	 * Warning when the site doesn't have Elementor installed or activated.
	 *
	 * @since 2.0.0
	 * @access public
	 */
	public function admin_notice_missing_elementor_plugin() {

		if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );

		$message = sprintf(
			/* translators: 1: Plugin name 2: Elementor */
			esc_html__( '"%1$s" requires "%2$s" to be installed and activated.', 'randomize' ),
			'<strong>' . esc_html__( 'Randomize Elementor Widget', 'randomize' ) . '</strong>',
			'<strong>' . esc_html__( 'Elementor', 'randomize' ) . '</strong>'
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );

	}

	/**
	 * Admin notice
	 * Warning when the site doesn't have a minimum required Elementor version.
	 *
	 * @since 2.0.0
	 * @access public
	 */
	public function admin_notice_minimum_elementor_version() {

		if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );

		$message = sprintf(
			/* translators: 1: Plugin name 2: Elementor 3: Required Elementor version */
			esc_html__( '"%1$s" requires "%2$s" version %3$s or greater.', 'randomize' ),
			'<strong>' . esc_html__( 'Randomize Elementor Widget', 'randomize' ) . '</strong>',
			'<strong>' . esc_html__( 'Elementor', 'randomize' ) . '</strong>',
			 self::MINIMUM_ELEMENTOR_VERSION
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );

	}

	/**
	 * Admin notice
	 * Warning when the site doesn't have a minimum required PHP version.
	 *
	 * @since 2.0.0
	 * @access public
	 */
	public function admin_notice_minimum_php_version() {

		if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );

		$message = sprintf(
			/* translators: 1: Plugin name 2: PHP 3: Required PHP version */
			esc_html__( '"%1$s" requires "%2$s" version %3$s or greater.', 'randomize' ),
			'<strong>' . esc_html__( 'Randomize Elementor Widget', 'randomize' ) . '</strong>',
			'<strong>' . esc_html__( 'PHP', 'randomize' ) . '</strong>',
			 self::MINIMUM_PHP_VERSION
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );

	}

	/**
	 * Initialize
	 * Load the addons functionality only after Elementor is initialized.
	 *
	 * Fired by `elementor/init` action hook.
	 *
	 * @since 2.0.0
	 * @access public
	 */
	public function init() {

		add_action( 'elementor/widgets/register', [ $this, 'register_widgets' ] );
		//add_action( 'elementor/controls/register', [ $this, 'register_controls' ] );

	}

	/**
	 * Register Widgets
     * 
	 * Load widgets files and register new Elementor widgets.
	 *
	 * Fired by `elementor/widgets/register` action hook.
	 *
	 * @param \Elementor\Widgets_Manager $widgets_manager Elementor widgets manager.
	 */
	public function register_widgets( $widgets_manager ) {

		//require_once( dirname( plugin_basename( __FILE__ ) ) . '/includes/elementor/widgets/elementor-randomize-text.php' );
		//require_once( dirname( plugin_basename( __FILE__ ) ) . '/includes/elementor/widgets/elementor-randomize-images.php' );

		//$widgets_manager->register( new RandomizeElementor_Text() );
		//$widgets_manager->register( new RandomizeElementor_Images() );

        require_once(  __DIR__ . '/widgets/elementor-randomize.php' );
        $widgets_manager->register( new Elementor_Randomize_Widget() );

	}

	/**
	 * Register Controls
     * 
	 * Load controls files and register new Elementor controls.
	 *
	 * Fired by `elementor/controls/register` action hook.
	 *
	 * @param \Elementor\Controls_Manager $controls_manager Elementor controls manager.
	 */
	public function register_controls( $controls_manager ) {

        //No controls to load...

		//require_once( __DIR__ . '/includes/controls/control-1.php' );
		//$controls_manager->register( new Control_1() );

	}

}